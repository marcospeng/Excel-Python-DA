本仓库代码源于『对比Excel，轻松学习Python数据分析』读者服务仓库，只是为了方便大家下载，所以在码云建了这个仓库。(**本人不是这本书的作者**)
> 文件夹说明：  
> Excel-Python-DA文件夹 为作者的代码随书代码  
> Note-One，Note-Two文件夹 为读者的随书代码笔记

# 作者在`github`原仓库地址：
```
https://github.com/junhongzhang/Excel-Python-DA
```

>以下是作者原文介绍

本仓库为『对比Excel，轻松学习Python数据分析』读者服务仓库。主要会包括以下几个方面：

# 本书介绍
[关于本书的详细介绍](https://gitee.com/xmaniu/Excel-Python-DA/blob/master/Excel-Python-DA/%E6%9C%AC%E4%B9%A6%E8%AF%A6%E7%BB%86%E4%BB%8B%E7%BB%8D.md)

# 随书代码
本模块为读者自由贡献板块，包含了书中所有的代码和数据集，大家可以到此查看。欢迎大家添加链接到此模块。

[书籍代码1](https://gitee.com/xmaniu/Excel-Python-DA/tree/master/Note-One)

[书籍代码2](https://gitee.com/xmaniu/Excel-Python-DA/tree/master/Note-Two)

# 本书勘误表
由于作者水平有限，所以在写作过程中难免会出现一些意想不到的错误，欢迎大家指正。
[勘误表](https://gitee.com/xmaniu/Excel-Python-DA/blob/master/Excel-Python-DA/%E5%8B%98%E8%AF%AF%E8%A1%A8.md)

# 随书数据集
为了能够让大家复现本书中的例子，现提供本书中用到的一些数据集。
[数据集](https://gitee.com/xmaniu/Excel-Python-DA/tree/master/Excel-Python-DA/%E9%9A%8F%E4%B9%A6%E6%95%B0%E6%8D%AE%E9%9B%86)

# 本书额外教程
针对大家问题比较多的问题或者大家在平常数据分析工作中有需求，但是本书没有涉及到的内容，我会专门来写相应的教程发出来。
[额外教程](https://gitee.com/xmaniu/Excel-Python-DA/tree/master/Excel-Python-DA/%E6%9C%AC%E4%B9%A6%E9%A2%9D%E5%A4%96%E6%95%99%E7%A8%8B)

# 本书所需Python安装包
可以直接到Anaconda下载所需要的Python安装包。[下载地址](https://www.anaconda.com/)
